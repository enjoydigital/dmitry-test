﻿using System.Web.Mvc;
using EnjoyDigitalUniversity.ViewModels;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace EnjoyDigitalUniversity.Controllers
{
    public class CourseListingController : RenderMvcController
    {
        public override ActionResult Index(RenderModel model)
        {
            var courseListing = new CourseListingViewModel(CurrentPage);



            TryUpdateModel(courseListing);

            return CurrentTemplate(courseListing);
        }
    }
}